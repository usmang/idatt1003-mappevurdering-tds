package edu.ntnu.stud;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit tests for the TrainDeparture class.
 *
 * <p>Test cases cover positive scenarios, since the class UserInterface
 * is responsible for handling user input and validation.
 * The TrainDeparture class assumes that it receives valid inputs.
 * Used ChatGPT for this test.
 *
 * @author 10006
 * @version 1.0
 * @since 0.1
 */
class TrainDepartureTest {

    /**
     * Test case: Retrieving the departure time of a TrainDeparture.
     * Positive test.
     */
    @Test
    void testGetDepartureTime() {
        TrainDeparture departure = new TrainDeparture("08:00", "L1", "101",
                "Station A", 1, "00:15");
        assertEquals("08:00", departure.getDepartureTime());
    }

    /**
     * Test case: Retrieving the line designation of a TrainDeparture.
     * Positive test.
     */
    @Test
    void testGetLine() {
        TrainDeparture departure = new TrainDeparture("08:00", "L1", "101",
                "Station A", 1, "00:15");
        assertEquals("L1", departure.getLine());
    }

    /**
     * Test case: Retrieving the train number of a TrainDeparture.
     * Positive test.
     */
    @Test
    void testGetTrainNumber() {
        TrainDeparture departure = new TrainDeparture("08:00", "L1", "101",
                "Station A", 1, "00:15");
        assertEquals("101", departure.getTrainNumber());
    }

    /**
     * Test case: Retrieving the destination of a TrainDeparture.
     * Positive test.
     */
    @Test
    void testGetDestination() {
        TrainDeparture departure = new TrainDeparture("08:00", "L1", "101",
                "Station A", 1, "00:15");
        assertEquals("Station A", departure.getDestination());
    }

    /**
     * Test case: Retrieving the track of a TrainDeparture.
     * Positive test.
     */
    @Test
    void testGetTrack() {
        TrainDeparture departure = new TrainDeparture("08:00", "L1", "101",
                "Station A", 1, "00:15");
        assertEquals("1", departure.getTrack());
    }

    /**
     * Test case: Retrieving the delay of a TrainDeparture.
     * Positive test.
     */
    @Test
    void testGetDelay() {
        TrainDeparture departure = new TrainDeparture("08:00", "L1", "101",
                "Station A", 1, "00:15");
        assertEquals("00:15", departure.getDelay());
    }

    /**
     * Test case: Setting the track of a TrainDeparture.
     * Positive test.
     */
    @Test
    void testSetTrack() {
        TrainDeparture departure = new TrainDeparture("08:00", "L1", "101",
                "Station A", 1, "00:15");
        departure.setTrack(2);
        assertEquals("2", departure.getTrack());
    }

    /**
     * Test case: Setting the delay of a TrainDeparture.
     * Positive test.
     */
    @Test
    void testSetDelay() {
        TrainDeparture departure = new TrainDeparture("08:00", "L1", "101",
                "Station A", 1, "00:15");
        departure.setDelay("01:30");
        assertEquals("01:30", departure.getDelay());
    }

    /**
     * Test case: Setting the delay of a TrainDeparture with null.
     * Positive test.
     */
    @Test
    void testSetDelayWithNull() {
        TrainDeparture departure = new TrainDeparture("08:00", "L1", "101",
                "Station A", 1, "00:15");
        departure.setDelay(null);
        assertEquals("00:00", departure.getDelay());
    }
}