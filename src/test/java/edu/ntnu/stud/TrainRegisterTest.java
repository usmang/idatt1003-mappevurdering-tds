package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertNull;
import org.junit.jupiter.api.Test;
import java.util.List;

/**
 * Unit tests for the TrainRegister class.
 *
 * <p>Test cases cover positive scenarios and negative scenarios.
 * Ensure that the TrainRegister class functions as expected and
 * handles different situations. Used ChatGPT for this test.
 *
 * @author 10006
 * @version 1.0
 * @since 0.2
 */
class TrainRegisterTest {

    /**
     * Test case: Adding a TrainDeparture to the TrainRegister.
     * Positive test.
     */
    @Test
    void testAddToTrainDeparture() {
        TrainRegister trainRegister = new TrainRegister();
        TrainDeparture departure1 = new TrainDeparture("08:00", "L1", "101",
                "Station A", 1, "00:15");

        trainRegister.addToTrainDeparture(departure1);

        List<TrainDeparture> departures = trainRegister.getTrainDepartures();
        assert departures.size() == 1 : "TrainDeparture not added successfully.";
    }

    /**
     * Test case: Searching for a TrainDeparture by train number.
     * Positive test.
     */
    @Test
    void testSearchTrainByNumber() {
        TrainRegister trainRegister = new TrainRegister();
        TrainDeparture departure1 = new TrainDeparture("08:00", "L1", "101",
                "Station A", 1, "00:15");
        trainRegister.addToTrainDeparture(departure1);

        TrainDeparture foundDeparture = trainRegister.searchTrainByNumber("101");

        assert foundDeparture != null && foundDeparture.getTrainNumber().equals("101") :
                "TrainDeparture not found by train number.";
    }

    /**
     * Test case: Updating the current system time.
     * Positive test.
     */
    @Test
    void testUpdateTime() {
        TrainRegister trainRegister = new TrainRegister();

        trainRegister.updateTime("12:30");

        assert trainRegister.getCurrentTime().equals("12:30") : "Current time not updated successfully.";
    }

    /**
     * Test case: Checking the validity of departure time for a TrainDeparture.
     * Positive test.
     */
    @Test
    void testIsValidDepartureTime() {
        TrainRegister trainRegister = new TrainRegister();
        TrainDeparture departure1 = new TrainDeparture("12:45", "L1", "101",
                "Station A", 1, "00:15");
        trainRegister.addToTrainDeparture(departure1);

        boolean isValidDepartureTime = trainRegister.isValidDepartureTime(departure1);

        assert isValidDepartureTime : "Valid departure time not recognized.";
    }

    /**
     * Test case: Searching for TrainDepartures by destination.
     * Negative test.
     */
    @Test
    void testSearchTrainByDestination() {
        TrainRegister trainRegister = new TrainRegister();
        TrainDeparture departure1 = new TrainDeparture("08:00", "L1", "101",
                "Station A", 1, "00:15");
        trainRegister.addToTrainDeparture(departure1);

        List<TrainDeparture> destinationDepartures = trainRegister.searchTrainByDestination("Station B");

        assert destinationDepartures.isEmpty() : "Unexpected TrainDepartures found for destination.";
    }

    /**
     * Test case: Searching for a non-existent TrainDeparture by train number.
     * Negative test.
     */
    @Test
    void testSearchNonExistentTrainByNumber() {
        TrainRegister trainRegister = new TrainRegister();
        TrainDeparture departure1 = new TrainDeparture("08:00", "L1", "101",
                "Station A", 1, "00:15");
        trainRegister.addToTrainDeparture(departure1);

        TrainDeparture foundDeparture = trainRegister.searchTrainByNumber("102");

        assertNull(foundDeparture, "Expected null for non-existent train number.");
    }
}