package edu.ntnu.stud;

import java.util.Arrays;

/**
 * The main class for the Train Dispatch Application.
 * This class serves as the entry point for the application and is responsible for
 * initializing the necessary components, such as the train register and user interface.
 * It starts the application by invoking the start method on the UserInterface.
 *
 * <p>The Train Dispatch Application allows operators at a train station to manage train departures,
 * including adding new departures, assigning tracks, adding delays, and searching for departures.
 * The application provides a text-based user interface with a menu-driven system.
 *
 * <p>This class is part of a larger system that includes:
 *
 * @see TrainDeparture
 * @see TrainRegister
 * @see UserInterface
 *
 * @author 10006
 * @version 1.0
 * @since 0.1
 */
public class TrainDispatchApp {

  /**
   * The main method to start the Train Dispatch Application.
   *
   * @param args The command-line arguments (not used in this application).
   */
  public static void main(String[] args) {
    TrainRegister trainRegister = new TrainRegister();
    UserInterface userInterface = new UserInterface(trainRegister);

    init(trainRegister);
    userInterface.start();
  }

  /**
   * Initializes the TrainRegister with sample TrainDeparture objects.
   *
   * <p>This method creates and adds a set of predefined TrainDeparture objects to the
   * TrainRegister. These objects represent sample train departures for demonstration
   * purposes.
   *
   * @param trainRegister The TrainRegister to be initialized.
   */
  private static void init(TrainRegister trainRegister) {

    TrainDeparture departure1 = new TrainDeparture("10:00", "L1", "123",
            "Oslo", -1, "00:00");
    TrainDeparture departure2 = new TrainDeparture("12:30", "L2", "456",
            "Trondheim", 2, "00:15");
    TrainDeparture departure3 = new TrainDeparture("14:45", "L3", "789",
            "Bergen", 1, "00:00");
    TrainDeparture departure4 = new TrainDeparture("16:20", "L4", "101",
            "Stavanger", -1, "00:30");

    for (TrainDeparture trainDeparture : Arrays.asList(departure1, departure2, departure3,
            departure4)) {
      trainRegister.addToTrainDeparture(trainDeparture);
    }

  }
}