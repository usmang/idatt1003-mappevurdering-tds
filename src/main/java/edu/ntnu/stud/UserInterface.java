package edu.ntnu.stud;

import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

/**
 * Represents the text-based user interface for interacting with train departures.
 *
 * <p>Gives the opportunity to perform various tasks related to managing train departures.
 *
 * <p>Uses a Scanner object to take user input and a TrainRegister object to manage train
 * departures. The menu options are presented to the user in a loop until the user chooses to exit.
 *
 * @see TrainRegister
 * @see TrainDispatchApp
 * @see TrainDeparture
 *
 * @author 10006
 * @version 1.0
 * @since 0.3
 */
public class UserInterface {
  private final TrainRegister trainRegister;
  private final Scanner scanner;

  /**
   * Constructor - creates a UserInterface instance with a given TrainRegister.
   *
   * @param trainRegister The TrainRegister used to manage train departures.
   */
  public UserInterface(TrainRegister trainRegister) {
    this.trainRegister = trainRegister;
    scanner = new Scanner(System.in);
  }

  /**
   * Method that starts the text-based user interface, presenting a menu to the user,
   * by calling on the {@link #displayMenu()} method, inside the while-loop.
   * The user can choose from various options to interact with train departures.
   * The loop continues until the user chooses to exit the application.
   */
  public void start() {
    while (true) {
      displayMenu();
      int choice = scanner.nextInt();
      scanner.nextLine();

      switch (choice) {
        case 1 -> displayTrainDepartures();
        case 2 -> addTrainDeparture(scanner);
        case 3 -> assignTrackToTrain(scanner);
        case 4 -> addDelayToTrain(scanner);
        case 5 -> searchTrainByNumber(scanner);
        case 6 -> searchTrainByDestination(scanner);
        case 7 -> updateTime(scanner);
        case 8 -> {
          System.out.println("Avslutter applikasjonen.");
          System.exit(0);
        }
        default -> System.out.println("Ugyldig valg. Prøv igjen.");
      }
    }
  }

  /**
   * Method that displays the menu options for the user to choose from, called by
   * the {@link #start()} method.
   */
  private void displayMenu() {
    System.out.println("\n  ========= M E N Y =========\n");
    System.out.println("  1 > Oversikt over togavganger");
    System.out.println("  2 > Legg til en ny togavgang");
    System.out.println("  3 > Legg til spor");
    System.out.println("  4 > Legg til forsinkelse");
    System.out.println("  5 > Søk etter tog, basert på tognummer");
    System.out.println("  6 > Søk etter tog, basert på destinasjon");
    System.out.println("  7 > Oppdater klokken");
    System.out.println("  8 > Avslutt");
    System.out.print("\nEnter valget ditt: ");
  }

  /**
   * Method that displays an overview of train departures.
   *
   * <p>The method retrieves the list of train departures from the TrainRegister,
   * sorts them by departure time, and prints a
   * formatted table to the console. The table includes columns for
   * departure time, line, train number, destination, delay, and track.
   *
   *<p>Column for track, from the TrainDeparture class, if -1
   * is set, is shown empty.
   * For delay, the ternary operator and isValidDepartureTime(departure) method from
   * TrainRegister class is used to set empty if "00:00" is given.
   */
  private void displayTrainDepartures() {
    List<TrainDeparture> departures = trainRegister.getTrainDepartures();
    departures.sort(Comparator.comparing(TrainDeparture::getDepartureTime));

    System.out.println("------------------------------------------------------------------"
             + "------------------------");
    System.out.printf("| %-8s | %-6s | %-12s | %-20s | %-15s | %-10s |\n",
            "Avgang", "Linje", "Tognr", "Destinasjon", "Forsinkelse", "Spor");
    System.out.println("---------------------------------------------------------------------"
             + "---------------------");

    for (TrainDeparture departure : departures) {
      if (trainRegister.isValidDepartureTime(departure)) {
        String delayToShow = departure.getDelay().equals("00:00") ? "" : departure.getDelay();
        System.out.printf("| %-8s | %-6s | %-12s | %-20s | %-15s | %-10s |\n",
                departure.getDepartureTime(), departure.getLine(), departure.getTrainNumber(),
                departure.getDestination(), delayToShow, departure.getTrack());
      }
    }

    System.out.println("------------------------------------------------------------------"
                + "------------------------");
  }

  /**
   * Method that adds a new train departure to the system, based on user input.
   *
   * <p>Guides the user through the process of adding a new train
   * departure, including the TrainDeparture attributes. Performs validation
   * checks, checking for input format, and ensuring data integrity by checking for
   * duplicate train numbers. If any input is invalid, appropriate error
   * messages are displayed, and the method exits early.
   * Otherwise, a new TrainDeparture object is created and added to the
   * TrainRegister.
   *
   * <p>Explanation of code flow:
   * - The method prompts the user for the departure time in the format HH:mm,
   * and validates it by applying a regular expression.
   * If the format is invalid, an error message is displayed, and the method exits.
   * - The user is prompted for the line, train number, and destination,
   * with additional validation for the train number to ensure it consists
   * of only digits.
   * - Duplicate train numbers are checked using the
   * TrainRegister's searchTrainByNumber method.
   * If a duplicate is found, an error message is displayed, and the method exits.
   * - For the track, the input allows to enter -1 if no track is assigned.
   * Both format (integer) and range (positive or -1) is validated.
   * The format of delay input, HH:mm, is checked.
   * - If all input is valid, a new TrainDeparture object is created
   * with the given information, and added to the TrainRegister.
   *
   * @param scanner The Scanner object used to collect user input.
   */
  private void addTrainDeparture(Scanner scanner) {
    System.out.println("LEGG TIL NY TOGAVGANG\n");

    System.out.print("Avgangstid (HH:mm): ");
    String departureTime = scanner.nextLine();
    if (!departureTime.matches("^([01]?[0-9]|2[0-3]):[0-5][0-9]$")) {
      System.out.println("""

              Feil-melding:
              Ugyldig format for avgangstid. Bruk formatet HH:mm.""");
      return;
    }

    System.out.print("Linje: ");
    String line = scanner.nextLine();

    System.out.print("Tognummer: ");
    String trainNumber = scanner.nextLine();

    if (!trainNumber.matches("\\d+")) {
      System.out.println("""
            
      Feil-melding:
      Ugyldig tognummer. Bruk kun tall.""");
      return;
    }

    if (trainRegister.searchTrainByNumber(trainNumber) != null) {
      System.out.println("""
            
      Feil-melding:
      Tognummeret eksisterer allerede.""");
      return;
    }

    System.out.print("Destinasjon: ");
    String destination = scanner.nextLine();

    int track = 0;
    System.out.print("Spor (-1 hvis ikke tildelt): ");
    while (true) {
      try {
        String trackInput = scanner.nextLine();

        if ("-1".equals(trackInput)) {
          track = -1;
          break;
        } else {
          int parsedTrack = Integer.parseInt(trackInput);

          if (parsedTrack < -1) {
            System.out.println("""

                                Feil-melding:
                                Ugyldig spor. Skriv -1 for å ikke tildele spor, eller
                                et positivt heltall for å tildele spor.""");
          } else {
            break;
          }
        }
      } catch (NumberFormatException e) {
        System.out.println("""

                        Feil-melding:
                        Ugyldig spor. Skriv -1 for å ikke tildele spor, eller
                        et positivt heltall for å tildele spor.""");
      }
    }

    System.out.print("Forsinkelse (HH:mm, Skriv 00:00 hvis ingen forsinkelse): ");
    String delayInput = scanner.nextLine();
    if (!delayInput.matches("^([01]?[0-9]|2[0-3]):[0-5][0-9]$")) {
      System.out.println("""

                Feil-melding:
                Ugyldig format for forsinkelse. Bruk formatet HH:mm.""");
      return;
    }

    TrainDeparture newDeparture = new TrainDeparture(departureTime, line, trainNumber,
            destination, track, delayInput);

    trainRegister.addToTrainDeparture(newDeparture);
    System.out.println("Togavgangen er lagt til.");
  }

  /**
   * Assigns a track to a train departure based on user input.
   *
   * <p>Explanation of code flow:
   * - The user is asked to enter a train number.
   * - The TrainRegister's searchTrainByNumber method is applied
   * to find the departure with the respective
   *   train number. If found, the user is asked to enter a track number.
   * - The entered track number is parsed as an integer,
   * if successful, it is then assigned to the found train departure's track property.
   * - Displays a confirmation message, if not a positive integer, displays an error.
   * - Prints error message if no train departure with the entered train number
   * is found.
   *
   * @param scanner The Scanner object used to collect user input.
   */
  private void assignTrackToTrain(Scanner scanner) {
    System.out.print("Søk etter et tog via tognummer: ");
    String trainNumber = scanner.nextLine();

    TrainDeparture foundDeparture = trainRegister.searchTrainByNumber(trainNumber);

    if (foundDeparture != null) {
      System.out.print("Tildel et spor (heltall): ");
      while (true) {
        try {
          int track = Integer.parseInt(scanner.nextLine());
          foundDeparture.setTrack(track);
          System.out.println("Sporet er tildelt til togavgangen.");
          break;
        } catch (NumberFormatException e) {
          System.out.println("""

                        Feil-melding:
                        Ugyldig spor. Skriv et positivt heltall for å tildele spor.""");
        }
      }
    } else {
      System.out.println("""

                Feil-melding:
                Ingen togavganger funnet med angitt tognummer.""");
    }
  }

  /**
   * Method that adds a delay to a train departure based on user input.
   *
   * <p>Explanation of code flow:
   * - The user is prompted to enter a train number.
   * - The TrainRegister's searchTrainByNumber method is used to
   * find the departure with the respective train number. If found, proceeds to
   * ask the user to enter a delay in the format HH:mm.
   * - Regular expression validates the entered delay, to ensure it matches
   * the expected format.
   * - If valid, delay is assigned to found train departure.
   * - Displays confirmation message after successful delay assignment, otherwise error
   * is shown.
   * -Prints an error message, if no train departure train number is matching with
   * the given train number.
   *
   * @param scanner The Scanner object used to collect user input.
   */
  private void addDelayToTrain(Scanner scanner) {
    System.out.print("Søk etter et tog via tognummer: ");
    String trainNumber = scanner.nextLine();

    TrainDeparture foundDeparture = trainRegister.searchTrainByNumber(trainNumber);

    if (foundDeparture != null) {
      System.out.print("Legg til forsinkelse (HH:mm, sett 00:00 hvis ingen forsinkelse): ");
      while (true) {
        String delayInput = scanner.nextLine();

        if (delayInput.matches("^([01]?[0-9]|2[0-3]):[0-5][0-9]$")) {
          foundDeparture.setDelay(delayInput);
          System.out.println("Forsinkelse lagt til togavgang.");
          break;
        } else {
          System.out.println("""

                        Feil-melding:
                        Ugyldig format for forsinkelse. Bruk formatet HH:mm.""");
        }
      }
    } else {
      System.out.println("""

                Feil-melding:
                Ingen togavganger funnet med angitt tognummer.""");
    }
  }

  /**
   * Method that searches for a train departure by its train number
   * and displays detailed information if found.
   *
   * <p>Explanation of code flow:
   * - Asks the user to provide a train number.
   * - searchTrainByNumber from the TrainRegister class is applied,
   * to find the departure with the specified train number.
   * - If a departure is found, detailed information about the departure is displayed.
   * - If no departure is found, an error message is displayed.
   *
   * @param scanner The Scanner object used to collect user input.
   */
  private void searchTrainByNumber(Scanner scanner) {
    System.out.print("Søk etter et tog via tognummer: ");
    String trainNumber = scanner.nextLine();

    TrainDeparture foundDeparture = trainRegister.searchTrainByNumber(trainNumber);

    if (foundDeparture != null) {
      System.out.println("Togavgang funnet:");
      System.out.println("Avgangstid: " + foundDeparture.getDepartureTime());
      System.out.println("Linje: " + foundDeparture.getLine());
      System.out.println("Tognummer: " + foundDeparture.getTrainNumber());
      System.out.println("Destinasjon: " + foundDeparture.getDestination());
      System.out.println("Spor: " + foundDeparture.getTrack());
      System.out.println("Forsinkelse: " + foundDeparture.getDelay());
    } else {
      System.out.println("""

                Feil-melding:
                Ingen togavganger funnet med angitt tognummer.""");
    }
  }

  /**
   * Method that searches for train departures
   * by destination and displays detailed information if found.
   *
   * <p>Explanation of code flow:
   * - The user is prompted to enter a destination.
   * - The TrainRegister's searchTrainByDestination method is used to
   * find departures with the specified destination.
   * - If departures are found, detailed information about each departure is displayed.
   * - If no departures are found, an error message is displayed.
   *
   * @param scanner The Scanner object used to collect user input.
   */
  private void searchTrainByDestination(Scanner scanner) {
    System.out.print("Søk etter et tog via destinasjon: ");
    String destination = scanner.nextLine();

    List<TrainDeparture> matchingDepartures = trainRegister.searchTrainByDestination(destination);

    if (!matchingDepartures.isEmpty()) {
      System.out.println("Togavganger funnet:");
      for (TrainDeparture departure : matchingDepartures) {
        System.out.println("Avgangstid: " + departure.getDepartureTime());
        System.out.println("Linje: " + departure.getLine());
        System.out.println("Tognummer: " + departure.getTrainNumber());
        System.out.println("Destinasjon: " + departure.getDestination());
        System.out.println("Spor: " + departure.getTrack());
        System.out.println("Forsinkelse: " + departure.getDelay());
        System.out.println("------------------------");
      }
    } else {
      System.out.println("""

                Feil-melding:
                Ingen togavganger funnet med angitt destinasjon.""");
    }
  }

  /**
   * System that updates the system time based on user input.
   *
   * <p>Explanation of code flow:
   * - Asks the user to enter a new time.
   * - The TrainRegister's updateTime method is
   * applied to validate and update the system time.
   * - If the entered time is valid, the system time is updated,
   * and a confirmation message is displayed.
   * - If the entered time is invalid, an error message is displayed.
   *
   * @param scanner The Scanner object used to collect user input.
   */
  private void updateTime(Scanner scanner) {
    System.out.print("Oppdater klokken (nytt tidspunkt HH:mm): ");
    String newTime = scanner.nextLine();

    trainRegister.updateTime(newTime);

    System.out.println("Klokken er oppdatert til " + trainRegister.getCurrentTime());
  }
}
