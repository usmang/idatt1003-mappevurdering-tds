package edu.ntnu.stud;

/**
 * Represents a train departure containing relevant information.
 *
 * <p>A train departure consists of:
 * - Departure time (HH:mm in 24-hour format)
 * - Line (e.g., "L1", "F4")
 * - Train number (unique within a 24-hour window)
 * - Destination
 * - Track (assigned track, set to -1 if not assigned)
 * - Delay (HH:mm format, set to "00:00" for no delay)
 *
 * <p>
 *  Field Data-types:
 * - {@link #departureTime} - datatype: "String"
 * Departure time is represented as a string in the format "HH:mm".
 * This format allows for easy validation and manipulation of
 * time strings within the application.
 * - {@link #line} - datatype: "String"
 * Line information is stored as a string, allowing
 * flexibility in representing different lines.
 * - {@link #trainNumber} - datatype: "String"
 * Train number is stored as a string to accommodate unique
 * identifiers within a 24-hour window.
 * The use of a string datatype aligns with the unique nature of train numbers and simplifies
 * validation in the application.
 * - {@link #destination} - datatype: "String"
 * Destination information is stored as a string, providing a straightforward representation
 * of the train's destination. The string datatype is suitable for handling
 * textual information.
 * - {@link #track} - datatype: "int"
 * Track information is stored as an integer to represent the assigned track.
 * An integer is chosen to allow for easy comparison and manipulation
 * of track numbers within the application.
 * - {@link #delay} - datatype: "String"
 * Delay information is stored as a string in the format "HH:mm".
 * Storing delays as strings simplifies the handling of time durations,
 * especially when dealing with potential differences in hours and minutes.
 *</p>
 *
 * <p>TrainDeparture objects are applied to model individual train departures within the
 * train dispatch system. These objects store departure time, line, train number,
 * destination, assigned track, and (if any) delays. TrainDeparture provides
 * methods to retrieve and modify this information.
 * Ensuring accurate representation and manipulation of train departure data.
 *
 * <p>TrainDeparture includes a constructor that creates instances of
 * TrainDeparture with initial information, and getter and setter methods
 * for accessing and updating the attributes.
 *
 * <p>The departureTime, line, trainNumber, and destination are mandatory fields -
 * (set upon creation of the instance through the constructor).
 * The track can be modified after creation using
 * {@link #setTrack(int)}, and the delay can be modified using
 * {@link #setDelay(String)} .
 *
 * <p>Constraints:
 * - Departure time must be in HH:mm format in a 24-hour window.
 * - Train number must be a unique positive int in a 24-hour window.
 * - Track is an integer, and -1 represents no assigned track.
 * - Delay must be in HH:mm format; "00:00" represents no delay.
 *
 * @see TrainRegister
 * @see UserInterface
 * @see TrainDispatchApp
 *
 * @author 10006
 * @version 1.0
 * @since 0.1
 */
public class TrainDeparture {
  private final String departureTime;
  private final String line;
  private final String trainNumber;
  private final String destination;
  private int track;
  private String delay;

  /**
    * Constructor - creates TrainDeparture with the following information.
    *
    * @param departureTime the departure time
    * @param line the line designation
    * @param trainNumber the unique train number
    * @param destination the destination of the train
    * @param track the assigned
    * @param delay the delay in HH:mm format
    */
  public TrainDeparture(String departureTime, String line, String trainNumber,
                        String destination, int track, String delay) {
    this.departureTime = departureTime;
    this.line = line;
    this.trainNumber = trainNumber;
    this.destination = destination;
    this.track = track;
    this.delay = delay;
  }

  /**
   * Gets the departure time of the train.
   *
   * @return the departure time in HH:mm format
   */
  public String getDepartureTime() {
    return departureTime;
  }

  /**
   * Gets the line designation of the train.
   *
   * @return the line designation
   */
  public String getLine() {
    return line;
  }

  /**
   * Gets the train number.
   *
   * @return the train number
   */
  public String getTrainNumber() {
    return trainNumber;
  }

  /**
   * Gets the destination of the train.
   *
   * @return the destination
   */
  public String getDestination() {
    return destination;
  }

  /**
   * Converts the given int track value to string representation, or returns an empty string.
   * The ternary operator checks if the provided track value is equal to -1.
   * If it is, an empty string is returned; otherwise,
   * the track's string representation is returned.
   *
   * @return Either the track's string representation or an empty string.
   */
  public String getTrack() {
    return (track == -1) ? "" : String.valueOf(track);
  }

  /**
   * Gets the delay of the train.
   *
   * @return the delay
   */
  public String getDelay() {
    return delay;
  }

  /**
   * Mutator-method: sets the track value for the object.
   * This method assigns track value to the object's internal state.
   *
   * @param track The new track value to be set.
   */
  public void setTrack(int track) {
    this.track = track;
  }

  /**
   * Mutator-method: sets the delay value for the object.
   * This method assigns the specified delay value to the object's internal state. If the
   * specified delay is not null, it is set as the new delay; otherwise, the default delay
   * "00:00" is assigned.
   *
   * @param delay The new delay value to be set. If null, the default value "00:00" is used.
   */
  public void setDelay(String delay) {
    this.delay = (delay != null) ? delay : "00:00";
  }
}