package edu.ntnu.stud;

import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;


/**
 * Manages a List of TrainDeparture objects;
 * providing methods to interact with the objects.
 *
 * <p>Offers methods to add new train departures, assign tracks,
 * add delays, search for departures by train number or destination,
 * update the time, and retrieve a list of train departures.
 *
 * <p>Constraints for methods as explained per TrainDeparture class,
 * except for updating time - which is taken care of
 * in this class,  are taken care of in UserInterface class.
 *
 * @see TrainDeparture
 * @see UserInterface
 * @see TrainDispatchApp
 *
 * @author 10006
 * @version 1.0
 * @since 0.2
 */

public class TrainRegister {
  private final List<TrainDeparture> trainDepartures;
  private String currentTime;

  /**
   * Constructor - creates TrainRegister with an empty list of
   * train departures and sets the initial time.
   *
   * <p>Since TrainRegister manages a collection of TrainDeparture objects, it is
   * helpful to use an ArrayList to store and organize the train departures.
   * ArrayList provides easy insertion, dynamic resizing and fast retrieval of elements.
   * Accordingly, ArrayList is a good solution for the requirements of the
   * Train Dispatch Application.
   */
  public TrainRegister() {
    this.trainDepartures = new ArrayList<>();
    this.currentTime = "00:00";
  }

  /**
   * Method that adds a TrainDeparture object to the list of train departures.
   *
   * @param departure The TrainDeparture object should be added to the list.
   */
  public void addToTrainDeparture(TrainDeparture departure) {
    trainDepartures.add(departure);
  }

  /**
   * Method that retrieves the list of TrainDeparture objects.
   *
   * @return A List containing TrainDeparture objects.
   */
  public List<TrainDeparture> getTrainDepartures() {
    return trainDepartures;
  }

  /**
   * Method that searches for a TrainDeparture by its train number.
   *
   * <p>Applies Java Streams to easily perform the search operation.
   * Iterates through the list of TrainDeparture objects associated with
   * the TrainRegister instance. It filters the departures based on the given train number,
   * comparing it with the train number of each departure. If a matching TrainDeparture
   * is found by the findFirst operation, it is returned; otherwise, null is returned.
   *
   * @param trainNumber The train number to search for.
   * @return The TrainDeparture object with the specified train number, or null if not found.
   */
  public TrainDeparture searchTrainByNumber(String trainNumber) {
    return trainDepartures.stream()
            .filter(departure -> departure.getTrainNumber()
                    .equals(trainNumber))
            .findFirst()
            .orElse(null);
  }

  /**
   *Method that searches for TrainDepartures by their destination.
   *
   * <p>This method also applies Java Streams.
   * Iterates through the list of TrainDeparture objects linked to
   * the TrainRegister instance. Filters the departures based on the provided destination,
   * performing a case-insensitive comparison with the destination of each departure.
   * The filtered list of matching TrainDepartures is then returned, by the toList operation.
   *
   * @param destination The destination to search for.
   * @return A List of TrainDeparture objects with the specified destination.
   */
  public List<TrainDeparture> searchTrainByDestination(String destination) {
    return trainDepartures.stream()
            .filter(departure -> departure.getDestination()
                    .equalsIgnoreCase(destination))
            .toList();
  }

  /**
   * Method that retrieves the current system time.
   *
   * @return The current system time in HH:mm format.
   */
  public String getCurrentTime() {
    return currentTime;
  }

  /**
   * Method that checks the validity of the provided time.
   *
   * <p>Verifies whether the given time string adheres to the valid 24-hour time format
   * (HH:mm). The regular expression pattern "^([01]?[0-9]|2[0-3]):[0-5][0-9]$" is used for the
   * validation. The pattern ensures that the hours part is between 00 and 23, and the minutes part
   * is between 00 and 59.
   *
   * @param time The time string to be validated.
   * @return True if the time string has a valid format, false otherwise.
   */
  private boolean isValidTimeFormat(String time) {
    return time.matches("^([01]?[0-9]|2[0-3]):[0-5][0-9]$");
  }

  /**
   * Method that checks if the provided time string is within time range (00:00 - 23:59).
   *
   * <p>Attempts to parse the given time string using the {@link LocalTime} class. If
   * parsing is successful, it then checks whether the parsed time is after 00:00 and before
   * 23:59, making sure that it is within the valid time range.
   *
   * <p>If parsing fails (due to an invalid time format), the method catches the
   * {@link DateTimeParseException} and returns false, indicating that the time string is not
   * within the valid time range.
   *
   * @param time The time string to be validated.
   * @return True if the time string is within the valid time range, false otherwise.
   */
  private boolean isValidTimeRange(String time) {
    try {
      LocalTime parsedTime = LocalTime.parse(time);
      return parsedTime.isAfter(LocalTime.parse("00:00")) && parsedTime
              .isBefore(LocalTime.parse("23:59"));
    } catch (DateTimeParseException e) {
      return false;
    }
  }

  /**
   * Method that checks if a TrainDeparture's scheduled departure time
   * (+delay) is after the current time.
   *
   * <p>Calculates the actual departure time by adding the delay to the
   * given departure time. Then, the method compares the calculated
   * departure time with the current system time to determine if the departure is still pending.
   *
   * <p>Parses the current system time and the scheduled departure time with the help
   * of {@link LocalTime} class. It then adds the delay to the departure time to obtain the
   * actual departure time for comparison.
   *
   * @param departure The TrainDeparture object to be checked.
   * @return True if the departure time is valid and pending, false otherwise.
   */
  public boolean isValidDepartureTime(TrainDeparture departure) {
    LocalTime currentTime = LocalTime.parse(this.currentTime);
    LocalTime departureTime = LocalTime.parse(departure.getDepartureTime());

    departureTime = departureTime.plusHours(Integer.parseInt(departure.getDelay().split(":")[0]))
            .plusMinutes(Integer.parseInt(departure.getDelay().split(":")[1]));

    return departureTime.isAfter(currentTime);
  }

  /**
   * Method that updates the current system time.
   *
   * <p>Tries to update the current system time with a given new time string (HH:mm).
   * The method uses two private helper methods, {@link #isValidTimeFormat(String)} and
   * {@link #isValidTimeRange(String)}, to perform the validation checks.
   * The new time is validated for the correct format and the valid
   * time range (00:00 - 23:59). If the validation passes, the current system time is updated;
   * otherwise, an error message is printed.
   *
   * @param newTime The new system time in HH:mm format.
   */
  public void updateTime(String newTime) {
    if (isValidTimeFormat(newTime) && isValidTimeRange(newTime)) {
      currentTime = newTime;
    } else {
      System.out.println("""

                 Feil-melding:
                 Ugyldig tidspunkt.
                 Bruk formatet HH:mm og sørg for at det er innenfor et gyldig
                 område (00:00-23:59).""");
    }
  }
}
