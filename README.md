# Portfolio project IDATA1003 - 2023
This file uses Mark Down syntax. For more information see [here](https://www.markdownguide.org/basic-syntax/).

STUDENT NAME = "Your name"  
STUDENT ID = "Your ID"

## Project description

[//]: # (This project aims to develop a user-friendly Train Dispatch System for station operators.)

## Project structure

[//]: # (I have one package which is edu.ntnu,stud., All the source files are stored at src. JUnit-test classes are stored at src>target>test-classes>edu>ntnu>stud.)

## Link to repository

[//]: # (git@gitlab.stud.idi.ntnu.no:usmang/idatt1003-mappevurdering-tds.git)

## How to run the project

[//]: # (Explained in the Rapport.)

## How to run the tests

[//]: # (Open the tests and run them.)

## References

[//]: # (Given in the Rapport.)
